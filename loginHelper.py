#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
elRepo.io

Copyright (C) 2020-2021  Gioacchino Mazzurco <gio@eigenlab.org>
Copyright (C) 2020-2021  Asociación Civil Altermundi <info@altermundi.net>

SPDX-License-Identifier: AGPL-3.0-only
"""
import retroshareJsonApi as rs
import rslog as log

DEFAULT_RS_PASSWORD = "0000"

def isLoggedIn():
	return rs.jsonApiCall("/rsLoginHelper/isLoggedIn")["retval"]

def attemptLogin(account, password = DEFAULT_RS_PASSWORD):
	return rs.jsonApiCall(
		"/rsLoginHelper/attemptLogin",
		{"account": account, "password": password} )["retval"]

def createLocation(
	name, password = DEFAULT_RS_PASSWORD,
	apiUser = rs.DEFAULT_API_USER, apiPass = rs.DEFAULT_API_PASSWORD ):
	log.dbg("createLocation", name, password, apiUser, apiPass)
	return rs.jsonApiCall(
		"/rsLoginHelper/createLocationV2",
		{
			"locationName": name,
			"pgpName": name,
			"password": password,
			"apiUser": apiUser,
			"apiPass": apiPass
		} )

def main():
	if not isLoggedIn():
		locationId = rs.jsonApiCall(
			"/rsLoginHelper/getLocations" )["locations"][0]["mLocationId"]
		print(attemptLogin(locationId))
	else:
		print("Already logged in")

if __name__ == '__main__':
    main()
