#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
elRepo.io

Copyright (C) 2020  Gioacchino Mazzurco <gio@eigenlab.org>
Copyright (C) 2020  Asociación Civil Altermundi <info@altermundi.net>

SPDX-License-Identifier: AGPL-3.0-only
"""

import json, requests, time, math, re

JSON_API_URL = "http://127.0.0.1:9092/"
JSON_API_USER = "buryTheDead"
JSON_API_PASSWORD = "inNodeCementery"

def jsonApiCall(
	function, params = None,
	baseUrl = JSON_API_URL,
	apiUser = JSON_API_USER, apiPassword = JSON_API_PASSWORD ):
	return requests.post(
		url = baseUrl + function,
		json = params,
		auth = ( apiUser, apiPassword ) ).json()

"""
you are subscribed to a group, which makes you a source for this group to your
friend nodes.
"""
GXS_GROUP_SUBSCRIBE_SUBSCRIBED = 0x04

"""Message is unread"""
GXS_MSG_STATUS_GUI_UNREAD  = 0x02

def main():
	mResp = jsonApiCall("/rsGxsForums/getForumsSummaries")
	if(not mResp["retval"]):
		print( "Failed forum summaries retrieval `retval`", mResp["retval"] )
		exit(-3)

	for forumMeta in mResp["forums"]:
		forumId = forumMeta["mGroupId"] 
		if(forumMeta["mSubscribeFlags"] & GXS_GROUP_SUBSCRIBE_SUBSCRIBED):
			mResp2 = jsonApiCall(
				"/rsGxsForums/getForumMsgMetaData",
				{ "forumId": forumId } )

			if(not mResp2["retval"]):
				print( "Failed message metadata retrieval `forumId`, `retval`",
						forumId, mResp2["retval"] )
				exit(-4)

			for msgMeta in mResp2["msgMetas"]:
				msgId = msgMeta["mMsgId"]

				print( "Marking unread message: ", msgId,
						msgMeta["mMsgName"].encode("utf8") )

				if( not (msgMeta["mMsgStatus"] & GXS_MSG_STATUS_GUI_UNREAD) ):
					jsonApiCall( "/rsGxsForums/markRead",
						{ "messageId":
							{
								"first": forumId,
								"second": msgId
							},
							"read": False
						} )

if __name__ == '__main__':
	main()
