#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
elRepo.io

Copyright (C) 2021  Gioacchino Mazzurco <gio@eigenlab.org>
Copyright (C) 2021  Asociación Civil Altermundi <info@altermundi.net>

SPDX-License-Identifier: AGPL-3.0-only
"""

import retroshareJsonApi as rs
from enum import Enum

class RetroshareInviteFlags(Enum):
    NOTHING           = 0x00
    CURRENT_IP        = 0x01
    FULL_IP_HISTORY   = 0x02
    DNS               = 0x04
    RADIX_FORMAT      = 0x08
    PGP_SIGNATURES    = 0x10

def getRetroshareInvite(
	sslId = "Own node",
	inviteFlags = RetroshareInviteFlags.DNS.value | RetroshareInviteFlags.CURRENT_IP.value ):
	return rs.jsonApiCall(
		"/rsPeers/GetRetroshareInvite",
		{"sslId": sslId, "inviteFlags": inviteFlags} )["retval"]

def acceptInvite(invite):
	return rs.jsonApiCall(
		"/rsPeers/acceptInvite",
		{"invite": invite} )["retval"]

def getFriendList():
	return rs.jsonApiCall("/rsPeers/getFriendList")["sslIds"]

def getOnlineList():
	return rs.jsonApiCall("/rsPeers/getOnlineList")["sslIds"]

def getPeerDetails(sslid):
	return rs.jsonApiCall(
		"/rsPeers/getPeerDetails", {"sslId": sslid} )["det"]

# RsPeerDetails: i
def printPeerInfo(i):
	print(i["gpg_id"], i["id"], i["name"], i["location"], i["connectAddr"])
