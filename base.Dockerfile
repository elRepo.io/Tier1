## Add +--build-arg FRESHCLONE=$(date +%s)+ to docker build commandline to
## force cloning a new
## To avoid caching add --build-arg CACHEBUST=$(date +%s)

## To prepare an image suitable as base for Gitlab CI use
# define_default_value CI_REGISTRY "registry.gitlab.com"
# define_default_value CI_IMAGE "elrepo.io/tier1"
# define_default_value CI_REGISTRY_IMAGE "${CI_REGISTRY}/${CI_IMAGE}"
# docker build -t "${CI_REGISTRY_IMAGE}:base" --build-arg KEEP_SOURCE=true --build-arg REPO_DEPTH="" -f base.Dockerfile .

## To push it to gitlab CI registry you need first to login and the to push
# docker login ${CI_REGISTRY}
# docker push ${CI_REGISTRY_IMAGE}:base

## Pushing can take a titanic amount of time so better do this on a machine with
## stable electricity, internet connection and no need to reboot soon

## To run the container
# docker run -it -p 127.0.0.1:9092:9092 "${CI_IMAGE}:base" \
# 	retroshare-service --jsonApiPort 9092 --jsonApiBindAddress 0.0.0.0

FROM ubuntu

ARG CACHEBUST=0

ARG INSTALL_CMD="apt-get update && apt-get upgrade --yes && apt-get install --yes --no-install-recommends"

# Multiline trick see https://stackoverflow.com/a/54397762
RUN \
	echo '\n\
set -e \n\
set -x \n\
apt-get update \n\
apt-get upgrade --yes \n\
apt-get install --yes --no-install-recommends $@ \n' > install_cmd ; \
	chmod +x install_cmd

# Avoid being prompted by tzdata package configuration later
# see https://stackoverflow.com/a/44333806
ENV DEBIAN_FRONTEND=noninteractive
RUN \
	ln -fs /usr/share/zoneinfo/UTC /etc/localtime ; \
	./install_cmd tzdata ; \
	dpkg-reconfigure --frontend noninteractive tzdata

# Install RetroShare build dependencies
RUN ./install_cmd \
		build-essential libssl-dev libbz2-dev libsqlite3-dev \
		libsqlcipher-dev libupnp-dev pkg-config libz-dev \
		qt5-qmake qt5-default libxapian-dev doxygen rapidjson-dev \
		git cmake

# Install JSON API cURL testing dependecies
RUN ./install_cmd curl
	
# Install JSON API Python testing dependecies
RUN ./install_cmd \
	 	python3-requests python3-pip && \
	pip3 install sseclient

# Install JSON API Dart testing dependecies
RUN ./install_cmd gnupg apt-transport-https wget && \
	sh -c 'wget -qO- https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -' && \
	sh -c 'wget -qO- https://storage.googleapis.com/download.dartlang.org/linux/debian/dart_stable.list > /etc/apt/sources.list.d/dart_stable.list' && \
	./install_cmd dart
ENV PATH "$PATH:/usr/lib/dart/bin"

# Cleanup no longer needed stuff
RUN apt-get autoremove --purge --yes && rm install_cmd
ENV DEBIAN_FRONTEND=

# Clone and compile RetroShare
ARG FRESHCLONE=0
ARG REPO_URL=https://gitlab.com/g10h4ck/RetroShare.git
ARG REPO_BRANCH=master
ARG REPO_DEPTH="--depth 2000"
ARG KEEP_SOURCE=false
RUN apt-get update && apt-get upgrade --yes
RUN git clone $REPO_DEPTH $REPO_URL -b $REPO_BRANCH && cd RetroShare && \
	git fetch --tags && cd ..
RUN \
	mkdir RetroShare-build && cd RetroShare-build && \
	qmake ../RetroShare \
		CONFIG+=no_retroshare_plugins \
		CONFIG+=no_retroshare_gui \
		CONFIG+=retroshare_service \
		CONFIG+=no_tests \
		CONFIG+=rs_jsonapi \
		CONFIG+=rs_deep_forums_index && \
	(make -j$(nproc) || make -j$(nproc) || make) && make install && \
	cd .. && rm -rf RetroShare-build && ($KEEP_SOURCE || rm -rf RetroShare)

# Just to populate dart global dependency cache
COPY ./ /tier1
ARG COMPILE_CMD='cd /tier1; rm -rf .dart_tool .packages build bin/*.exe; dart pub get; dart pub upgrade; for entryP in bin/*.dart; do dart compile exe $entryP ; done'
RUN bash -c "$COMPILE_CMD"
RUN rm -rf /tier1
