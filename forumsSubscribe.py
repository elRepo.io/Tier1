#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
elRepo.io

Copyright (C) 2020-2021  Gioacchino Mazzurco <gio@eigenlab.org>
Copyright (C) 2020-2021  Asociación Civil Altermundi <info@altermundi.net>

SPDX-License-Identifier: AGPL-3.0-only
"""

import retroshareJsonApi as rs
import sys

FORUM_MAX_STORAGE_TIME = 0xFFFFFFFF
FORUM_MAX_SYNC_TIME = 0xFFFFFFFF

def subscribeForum(forumId, subscribe = True):
	rs.jsonApiCall(
				"/rsGxsForums/subscribeToForum",
				{ "forumId": forumId, "subscribe": subscribe } )

def setStorageTime(forumId, storageSecs = FORUM_MAX_STORAGE_TIME):
	rs.jsonApiCall(
				"/rsGxsForums/setStoragePeriod",
				{ "groupId": forumId, "storageSecs": storageSecs } )

def setSyncTime(forumId, syncSecs = FORUM_MAX_SYNC_TIME):
	rs.jsonApiCall(
				"/rsGxsForums/setSyncPeriod",
				{ "groupId": forumId, "syncAge": syncSecs } )

def main():
	subscribeForum(sys.argv[1])

if __name__ == '__main__':
    main()
