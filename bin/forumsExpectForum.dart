/*
 * elRepo.io
 * 
 * Copyright (C) 2021-2022  Gioacchino Mazzurco <gio@eigenlab.org>
 * Copyright (C) 2021-2022  Asociación Civil Altermundi <info@altermundi.net>
 * 
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'dart:io';
import 'dart:cli';

import 'package:retroshare_dart_wrapper/retroshare.dart';
import 'package:tier1_utils/tier1_env.dart';


int main(List<String> arguments)
{
  if(arguments.length != 2)
  {
    dbg('Needs <forum id> <timeout in seconds>');
    return -1;
  }

  var forumId = arguments[0];
  var timeoutAfter = int.parse(arguments[1]);
  var startTime = DateTime.now();
  var timeout = startTime.add(Duration(seconds: timeoutAfter));

  tier1_env_setup();

  var found = false;
  do
  {
    var forumsSummaries;
    try { forumsSummaries = waitFor(RsGxsForum.getForumsInfo([forumId])); }
    catch(e) { sleep(Duration(milliseconds: 500)); continue; }

    if(forumsSummaries.isNotEmpty)
    {
      found = true;
      var meta = forumsSummaries[0]['mMeta'];
      print('Forum propagated after ${DateTime.now().difference(startTime).inSeconds} seconds ${meta["mGroupId"]} ${meta["mPublishTs"]["xstr64"]} ${meta["mGroupName"]}');
    }
  }
  while(!found && DateTime.now().isBefore(timeout));

  if(found) return 0;

  dbg('Timeout after waiting for $forumId $timeoutAfter seconds');
  return -2;
}
