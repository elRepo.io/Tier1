/*
 * elRepo.io
 * 
 * Copyright (C) 2021-2022  Gioacchino Mazzurco <gio@eigenlab.org>
 * Copyright (C) 2021-2022  Asociación Civil Altermundi <info@altermundi.net>
 * 
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'dart:io';
import 'dart:cli';

import 'package:retroshare_dart_wrapper/retroshare.dart';
import 'package:tier1_utils/tier1_env.dart';


int main(List<String> arguments)
{
  if(arguments.length != 2)
  {
    dbg('Needs <peer id> <timeout in seconds>');
    return -1;
  }

  var peerId = arguments[0];
  var timeoutAfter = int.parse(arguments[1]);
  var startTime = DateTime.now();
  var timeout = startTime.add(Duration(seconds: timeoutAfter));

  tier1_env_setup();

  var isOnline = false;
  do
  {
    isOnline = waitFor(RsPeers.isOnline(peerId));
    if(!isOnline) { sleep(Duration(milliseconds: 500)); }
  }
  while(!isOnline && DateTime.now().isBefore(timeout));

  if(isOnline)
  {
    print('Peer $peerId connected after ${DateTime.now().difference(startTime).inSeconds} seconds');
    return 0;
  }

  dbg('Timeout after waiting for $peerId $timeoutAfter seconds');
  return -2;
}
