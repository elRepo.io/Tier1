/*
 * elRepo.io
 * 
 * Copyright (C) 2021-2022  Gioacchino Mazzurco <gio@eigenlab.org>
 * Copyright (C) 2021-2022  Asociación Civil Altermundi <info@altermundi.net>
 * 
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'dart:io';
import 'dart:cli';

import 'package:retroshare_dart_wrapper/retroshare.dart';
import 'package:tier1_utils/tier1_env.dart';


int main(List<String> arguments)
{
  if(arguments.length != 3)
  {
    dbg('Needs <forum id> <post id> <timeout in seconds>');
    return -1;
  }

  var forumId = arguments[0];
  var postId =  arguments[1];
  var timeoutAfter = int.parse(arguments[2]);
  var startTime = DateTime.now();
  var timeout = startTime.add(Duration(seconds: timeoutAfter));

  tier1_env_setup();

  var found = false;
  do
  {
    var posts;
    try { posts = waitFor(RsGxsForum.getForumContent(forumId, [postId])); }
    catch(_e) { /* No need to handle this */ };

    if(posts.isNotEmpty && posts[0]['mMeta']['mMsgId'] == postId)
    {
      found = true;
      var meta = posts[0]['mMeta'];
      print('Post propagated after ${DateTime.now().difference(startTime).inSeconds} seconds ${meta["mMsgId"]} ${meta["mPublishTs"]["xstr64"]} ${meta["mMsgName"]}');
      break;
    }

    sleep(Duration(milliseconds: 500));
  }
  while(!found && DateTime.now().isBefore(timeout));

  if(found) return 0;

  dbg('Timeout after waiting for $forumId $timeoutAfter seconds');
  return -2;
}
