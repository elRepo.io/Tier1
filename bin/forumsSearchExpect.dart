/*
 * elRepo.io
 * 
 * Copyright (C) 2021-2022  Gioacchino Mazzurco <gio@eigenlab.org>
 * Copyright (C) 2021-2022  Asociación Civil Altermundi <info@altermundi.net>
 * 
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'dart:io';
import 'dart:async';

import 'package:retroshare_dart_wrapper/retroshare.dart';
import 'package:retroshare_dart_wrapper/rs_models.dart';
import 'package:tier1_utils/tier1_env.dart';


int main(List<String> arguments)
{
  if(arguments.length != 1)
  {
    dbg('Needs <match string>');
    return -1;
  }

  var matchString = arguments[0];
  var startTime = DateTime.now();

  tier1_env_setup();

  RsGxsForum.distantSearchRequest(matchString).then(
    (searchId)
    {
      RsEvents.registerEventsHandler(
        RsEventType.GXS_FORUMS,
        (StreamSubscription stream, Map<String, dynamic> event)
        {
          if( event['mForumEventCode'] ==
                RsForumEventCode.DISTANT_SEARCH_RESULT.index
                && event['mSearchId'] == searchId )
          {
            print(
              'Found matching result after '
              '${DateTime.now().difference(startTime).inSeconds} seconds '
              '${event['mSearchResults'][0]['mSearchContext']}' );
            stream.cancel();
            stream = null;
            exit(0);
          }
        }
      );
    });

  return -2;
}
