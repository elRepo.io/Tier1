#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
elRepo.io

Copyright (C) 2020  Gioacchino Mazzurco <gio@eigenlab.org>
Copyright (C) 2020  Asociación Civil Altermundi <info@altermundi.net>

SPDX-License-Identifier: AGPL-3.0-only
"""

import retroshareJsonApi as rs
from rsgxsforums import RsForumEventCode_NEW_FORUM, RsForumEventCode_NEW_MESSAGE
from rsevents import RsEventType_GXS_CIRCLES, RsEventType_GXS_FORUMS
from rsgxscircles import *
from scriptPath import scriptPath
import subprocess

def main():
	for event in rs.getEventsStream():
		# Call handlears as subprocess instead of including and calling the
		# function, so once terminated the sockets are closed, avoiding
		# timeout problems due to request library keeping them open
		# after finishing the request.

		if(event["mType"] == RsEventType_GXS_CIRCLES):
			invitedType = RsGxsCircleEventCode.CIRCLE_MEMBERSHIP_ID_ADDED_TO_INVITEE_LIST
			if(event["mCircleEventType"] == invitedType.value):
				mScript = scriptPath()+"/bin/circlesJoinCircle.exe"
				try:
					print( "Subscribing to circle:", event["mCircleId"],
							"with invited identity:", event["mGxsId"] )
					subprocess.run([mScript, event["mCircleId"], event["mGxsId"]])
				except Exception as e:
					print( "Failend handing circle to: ",
							mScript, event["mCircleId"], event["mGxsId"], e )
				continue

		if(event["mType"] == RsEventType_GXS_FORUMS):
			if(event["mForumEventCode"] == RsForumEventCode_NEW_MESSAGE):
				mScript = scriptPath()+"/forumsNewPostHandler.py"
				try: subprocess.run(
					[mScript, event["mForumGroupId"], event["mForumMsgId"]] )
				except Exception as e:
					print( "Failure handing new message to:",
							mScript, event["mForumGroupId"],
							event["mForumMsgId"], e )
					continue

			if(event["mForumEventCode"] == RsForumEventCode_NEW_FORUM):
				mScript = scriptPath()+"/forumsNewForumHandler.py"
				try: subprocess.run([mScript, event["mForumGroupId"]])
				except Exception as e:
					print( "Failend handing new forum to:",
							mScript, event["mForumGroupId"], e )
					continue

if __name__ == '__main__':
	main()
