#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
elRepo.io

Copyright (C) 2020  Gioacchino Mazzurco <gio@eigenlab.org>
Copyright (C) 2020  Asociación Civil Altermundi <info@altermundi.net>

SPDX-License-Identifier: AGPL-3.0-only
"""

import json, requests, time, math, re

JSON_API_URL = "http://127.0.0.1:9092/"
JSON_API_USER = "buryTheDead"
JSON_API_PASSWORD = "inNodeCementery"

def jsonApiCall(
	function, params = None,
	baseUrl = JSON_API_URL,
	apiUser = JSON_API_USER, apiPassword = JSON_API_PASSWORD ):
	return requests.post(
		url = baseUrl + function,
		json = params,
		auth = ( apiUser, apiPassword ) ).json()


""" You have the admin key for this group """
GXS_GROUP_SUBSCRIBE_ADMIN = 0x01

"""
You have the publish key for thiss group. Typical use: publish key in
channels are shared with specific friends.
"""
GXS_GROUP_SUBSCRIBE_PUBLISH = 0x02

"""
you are subscribed to a group, which makes you a source for this group to your
friend nodes.
"""
GXS_GROUP_SUBSCRIBE_SUBSCRIBED = 0x04

"""You are not subscribed"""
GXS_GROUP_SUBSCRIBE_NOT_SUBSCRIBED = 0x08

"""Message is unread"""
GXS_MSG_STATUS_GUI_UNREAD  = 0x02

"""Message is new"""
GXS_MSG_STATUS_GUI_NEW = 0x04

URL_PATTERN = re.compile('[A-Za-z+-]+://[^"<>\^`{|}\s]+')

def main():
	mResp = jsonApiCall("/rsGxsForums/getForumsSummaries")
	if(not mResp["retval"]):
		print( "Failed forum summaries retrieval `retval`", mResp["retval"] )
		exit(-3)

	for forumMeta in mResp["forums"]:
		forumId = forumMeta["mGroupId"] 
		if(forumMeta["mSubscribeFlags"] & GXS_GROUP_SUBSCRIBE_NOT_SUBSCRIBED):
			print(
				"Subscribing forum:",
				forumId, forumMeta["mGroupName"].encode("utf8") )
			jsonApiCall(
				"/rsGxsForums/subscribeToForum",
				{ "forumId": forumId, "subscribe": True } )
		else:
			mResp2 = jsonApiCall(
				"/rsGxsForums/getForumMsgMetaData",
				{ "forumId": forumId } )

			if(not mResp2["retval"]):
				print( "Failed message metadata retrieval `forumId`, `retval`",
						forumId, mResp2["retval"] )
				exit(-4)

			newMessagesIds = []
			
			for msgMeta in mResp2["msgMetas"]:
				if( msgMeta["mMsgStatus"] & (
					GXS_MSG_STATUS_GUI_NEW | GXS_MSG_STATUS_GUI_UNREAD ) ):
					msgId = msgMeta["mMsgId"]
					print( "Found new message: ", msgId,
							msgMeta["mMsgName"].encode("utf8") )
					newMessagesIds.append(msgId)

			if(len(newMessagesIds) > 0):
				mResp2 = jsonApiCall(
					"/rsGxsForums/getForumContent",
					{ "forumId": forumId, "msgsIds": newMessagesIds } )

				if(not mResp2["retval"]):
					print( "Failed message data retrieval `forumId`, `retval`",
							forumId, mResp2["retval"] )
					exit(-5)
			else: continue

			scannedUrls = []

			for mMsg in mResp2["msgs"]:
				for sUrl in re.findall(URL_PATTERN, mMsg["mMsg"]):
					scannedUrls.append(sUrl)
				jsonApiCall( "/rsGxsForums/markRead",
					{ "messageId":
						{
							"first":  mMsg["mMeta"]["mGroupId"],
							"second": mMsg["mMeta"]["mMsgId"]
						},
						"read":True
					} )

			for fUrl in scannedUrls:
				mResp5 = jsonApiCall(
					"/rsFiles/parseFilesLink", { "link": fUrl } )
				print(fUrl.encode("utf8"), mResp5["retval"])
				if(mResp5["retval"]["errorNumber"] != 0): continue

				print(
					jsonApiCall( "/rsFiles/requestFiles",
								 { "collection": mResp5["collection"] } ))


if __name__ == '__main__':
    main()
