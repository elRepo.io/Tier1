FROM registry.gitlab.com/elrepo.io/tier1:latest

COPY ./ /tier1
ARG COMPILE_CMD='cd /tier1; rm -rf .dart_tool .packages build bin/*.exe; dart pub get; dart pub upgrade; for entryP in bin/*.dart; do dart compile exe $entryP ; done'
RUN bash -c "$COMPILE_CMD"
