#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
elRepo.io

Copyright (C) 2021  Gioacchino Mazzurco <gio@eigenlab.org>
Copyright (C) 2021  Asociación Civil Altermundi <info@altermundi.net>

SPDX-License-Identifier: AGPL-3.0-only
"""

import time
from enum import Enum

class LogCategories(Enum):
	DEBUG   = 'D'
	INFO    = 'I'
	WARNING = 'W'
	ERROR   = 'E'
	FATAL   = 'F'

def _Logger(category: LogCategories, *args):
	print(category.value, time.time(), args)

def dbg(*args):
	_Logger(LogCategories.DEBUG, args)

def info(*args):
	_Logger(LogCategories.INFO, args)

def warn(*args):
	_Logger(LogCategories.WARNING, args)

def err(*args):
	_Logger(LogCategories.ERROR, args)

def fatal(*args):
	_Logger(LogCategories.FATAL, args)
