FROM registry.gitlab.com/elrepo.io/tier1:base

ARG CACHEBUST=0

RUN apt-get update -y && apt-get upgrade -y

ARG REPO_URL=https://gitlab.com/g10h4ck/RetroShare.git
ARG REPO_BRANCH=elrepo.io
RUN \
	cd RetroShare && git remote add testing $REPO_URL && \
	git fetch --tags testing $REPO_BRANCH && \
	git reset --hard testing/$REPO_BRANCH
RUN \
	mkdir RetroShare-build && cd RetroShare-build && \
	qmake ../RetroShare \
		CONFIG+=debug \
		CONFIG+=no_retroshare_plugins \
		CONFIG+=no_retroshare_gui \
		CONFIG+=retroshare_service \
		CONFIG+=no_tests \
		CONFIG+=rs_jsonapi \
		CONFIG+=rs_deep_forums_index CONFIG+=no_sqlcipher && \
	(make -j$(nproc) || make -j$(nproc) || make) && make install && \
	cd .. && rm -rf RetroShare-build

# Just to populate dart global dependency cache
COPY ./ /tier1
ARG COMPILE_CMD='cd /tier1; rm -rf .dart_tool .packages build bin/*.exe; dart pub get; dart pub upgrade; for entryP in bin/*.dart; do dart compile exe $entryP ; done'
RUN bash -c "$COMPILE_CMD"
RUN rm -rf /tier1
