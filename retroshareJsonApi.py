#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
elRepo.io

Copyright (C) 2020-2021  Gioacchino Mazzurco <gio@eigenlab.org>
Copyright (C) 2020-2021  Asociación Civil Altermundi <info@altermundi.net>

SPDX-License-Identifier: AGPL-3.0-only
"""

import json, requests
import rslog as log

DEFAULT_API_URL      = "http://127.0.0.1:9092/"
DEFAULT_API_USER     = "buryTheDead"
DEFAULT_API_PASSWORD = "inNodeCementery"

def jsonApiCall(
	function, params = None,
	baseUrl = DEFAULT_API_URL,
	apiUser = DEFAULT_API_USER,
	apiPassword = DEFAULT_API_PASSWORD ):
	mResponse = requests.post(
		url = baseUrl + function,
		json = params,
		auth = ( apiUser, apiPassword ) )
	if not mResponse.ok: raise mResponse.raise_for_status()
	return mResponse.json()

"""RetroShare do not filter events"""
RsEventType___NONE = 0

def getEventsStream(
	eventType = RsEventType___NONE,
	baseUrl = DEFAULT_API_URL,
	apiUser = DEFAULT_API_USER,
	apiPassword = DEFAULT_API_PASSWORD ):

	from sseclient import SSEClient
	for mRecord in SSEClient(
			baseUrl+"/rsEvents/registerEventsHandler",
			auth = (apiUser, apiPassword),
			json = { "eventType": eventType } ):
		try:
			mEvent = json.loads(mRecord.data)["event"]
			## Older RetroShare version doesn't filter events type 
			if(mEvent["mType"] == eventType or eventType == RsEventType___NONE):
				yield mEvent
		except(KeyError):
			if json.loads(mRecord.data)["retval"]: continue
		except:
			print("Got invalid record:", mRecord.dump().encode("utf8"))
