#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
elRepo.io

Copyright (C) 2020  Gioacchino Mazzurco <gio@eigenlab.org>
Copyright (C) 2020  Asociación Civil Altermundi <info@altermundi.net>

SPDX-License-Identifier: AGPL-3.0-only
"""

from inspect import currentframe, getframeinfo
from pathlib import Path

def scriptPath():
	filename = getframeinfo(currentframe()).filename
	return str(Path(filename).resolve().parent)
