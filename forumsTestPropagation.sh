#!/bin/bash

<<"COPYLEFT"

Copyright (C) 2021  Gioacchino Mazzurco <gio@eigenlab.org>
Copyright (C) 2021  Asociación Civil Altermundi <info@altermundi.net>

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the
Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along
with this program. If not, see <https://www.gnu.org/licenses/>

COPYLEFT

## Define default value for variable, take two arguments, $1 variable name,
## $2 default variable value, if the variable is not already define define it
## with default value.
function define_default_value()
{
	VAR_NAME="${1}"
	DEFAULT_VALUE="${2}"

	[ -z "${!VAR_NAME}" ] && export ${VAR_NAME}="${DEFAULT_VALUE}" || true
}

define_default_value CI_REGISTRY "registry.gitlab.com"
define_default_value CI_IMAGE "elrepo.io/tier1"
define_default_value CI_REGISTRY_IMAGE "${CI_REGISTRY}/${CI_IMAGE}"

define_default_value NN1 tier1_test_node1
define_default_value NN2 tier1_test_node2
define_default_value NN3 tier1_test_node3
define_default_value NN4 tier1_test_node4

docker build -t "$CI_REGISTRY_IMAGE:test-forum-propagation" \
    -f test-forum-propagation.Dockerfile .

function cleanup
{
  echo "Stopping containers"
  docker container stop $NN1 $NN2 $NN3 $NN4 || true
}
trap cleanup EXIT

docker run --rm --name $NN1 --detach --tty \
    "$CI_REGISTRY_IMAGE:test-forum-propagation" \
    bash -c "retroshare-service --jsonApiPort 9092 &> retroshare.log"
docker run --rm --name $NN2 --detach --tty \
    "$CI_REGISTRY_IMAGE:test-forum-propagation" \
    bash -c "retroshare-service --jsonApiPort 9092 &> retroshare.log"
docker run --rm --name $NN3 --detach --tty \
    "$CI_REGISTRY_IMAGE:test-forum-propagation" \
    bash -c "retroshare-service --jsonApiPort 9092 &> retroshare.log"
docker run --rm --name $NN4 --detach --tty \
    "$CI_REGISTRY_IMAGE:test-forum-propagation" \
    bash -c "retroshare-service --jsonApiPort 9092 &> retroshare.log"

docker exec $NN1 /tier1/bin/jsonapiPrintVersion.exe

docker exec $NN1 /tier1/locationCreate.py $NN1
docker exec $NN2 /tier1/locationCreate.py $NN2
docker exec $NN3 /tier1/locationCreate.py $NN3
docker exec $NN4 /tier1/locationCreate.py $NN4
NN1_ID="$(docker exec $NN1 /tier1/bin/accountsGetDefaultLocation.exe)"
NN2_ID="$(docker exec $NN2 /tier1/bin/accountsGetDefaultLocation.exe)"
NN3_ID="$(docker exec $NN3 /tier1/bin/accountsGetDefaultLocation.exe)"
NN4_ID="$(docker exec $NN4 /tier1/bin/accountsGetDefaultLocation.exe)"

docker exec $NN1 /tier1/peersGetInvite.py > /tmp/$NN1
docker exec $NN2 /tier1/peersGetInvite.py > /tmp/$NN2
docker exec $NN3 /tier1/peersGetInvite.py > /tmp/$NN3
docker exec $NN4 /tier1/peersGetInvite.py > /tmp/$NN4

echo "Connecting $NN1 <-> $NN2"
docker exec -i $NN1 /tier1/peersAcceptInvite.py < /tmp/$NN2
docker exec -i $NN2 /tier1/peersAcceptInvite.py < /tmp/$NN1

echo "Waiting for connection to happen"
docker exec $NN1 /tier1/bin/peersExpectOnline.exe "$NN2_ID" 100;
docker exec $NN2 /tier1/bin/peersExpectOnline.exe "$NN1_ID" 100;

echo 'Test propagation of newly created forum'
FORUM_NAME="$NN1 test forum $RANDOM"
FORUM_ID="$(docker exec $NN1 /tier1/forumCreate.py "$FORUM_NAME" "$RANDOM description")"
echo "Waiting for forum '$FORUM_NAME' to propagate"
docker exec $NN2 /tier1/bin/forumsExpectForum.exe $FORUM_ID 300

echo 'Test propagation of post created before subscription'
AUTHOR_NAME="$NN1 test author $RANDOM"
POST_NAME="$NN1 test post $RANDOM"
AUTHOR_ID="$(docker exec $NN1 /tier1/bin/identitiesCreate.exe "$AUTHOR_NAME")"
POST_TITLE="Post from $NN1"
POST_ID="$(docker exec $NN1 /tier1/bin/forumsCreatePost.exe $FORUM_ID $AUTHOR_ID "$POST_TITLE" "$RANDOM $RANDOM $RANDOM")"
docker exec $NN2 /tier1/forumsSubscribe.py $FORUM_ID
docker exec $NN2 /tier1/bin/forumsExpectPost.exe $FORUM_ID $POST_ID 300

echo 'Test propagation of post created after subscription'
AUTHOR_NAME="$NN2 test author $RANDOM"
POST_NAME="$NN2 test post $RANDOM"
AUTHOR_ID="$(docker exec $NN2 /tier1/bin/identitiesCreate.exe "$AUTHOR_NAME")"
POST2_ID="$(docker exec $NN2 /tier1/bin/forumsCreatePost.exe $FORUM_ID $AUTHOR_ID "Post from $NN2" "Body $RANDOM $RANDOM $RANDOM")"
docker exec $NN1 /tier1/bin/forumsExpectPost.exe $FORUM_ID $POST2_ID 300

echo "Connecting $NN2 <-> $NN3 <-> $NN4"
docker exec -i $NN2 /tier1/peersAcceptInvite.py < /tmp/$NN3
docker exec -i $NN3 /tier1/peersAcceptInvite.py < /tmp/$NN2
docker exec -i $NN3 /tier1/peersAcceptInvite.py < /tmp/$NN4
docker exec -i $NN4 /tier1/peersAcceptInvite.py < /tmp/$NN3

echo "Waiting for connection to happen"
docker exec $NN2 /tier1/bin/peersExpectOnline.exe "$NN3_ID" 100;
docker exec $NN3 /tier1/bin/peersExpectOnline.exe "$NN4_ID" 100;

echo "Waiting search result for $POST_TITLE"
docker exec $NN4 timeout 20s /tier1/bin/forumsSearchExpect.exe "$POST_TITLE";
