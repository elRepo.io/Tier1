#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
elRepo.io

Copyright (C) 2020  Gioacchino Mazzurco <gio@eigenlab.org>
Copyright (C) 2020  Asociación Civil Altermundi <info@altermundi.net>

SPDX-License-Identifier: AGPL-3.0-only
"""

import retroshareJsonApi as rs

"""
you are subscribed to a group, which makes you a source for this group to your
friend nodes.
"""
GXS_GROUP_SUBSCRIBE_SUBSCRIBED = 0x04

def main():
	mResp = rs.jsonApiCall("/rsGxsForums/getForumsSummaries")
	if(not mResp["retval"]):
		print( "Failed forum summareis retrieval `retval`", mResp["retval"] )
		exit(-3)

	for forumMeta in mResp["forums"]:
		if(forumMeta["mSubscribeFlags"] & GXS_GROUP_SUBSCRIBE_SUBSCRIBED):
			print(
				"Unsubscribing forum:",
				forumMeta["mGroupId"], forumMeta["mGroupName"].encode("utf8") )
			rs.jsonApiCall(
				"/rsGxsForums/subscribeToForum",
				{ "forumId": forumMeta["mGroupId"], "subscribe": False } )

if __name__ == '__main__':
    main()
